# snake-game-js

A simple snake game using pure javascript. Run it on a browser and use your arrow keys or WASD to move around the area. Eat the food for the snake to grow.

I developed this from scratch and I didn't base the code from anywhere so I'm not sure if my code is optimized or correct but it was fun coding this.

## Controls
`W` or `Arrow Up` - go up \
`A` or `Arrow Left` - go left \
`S` or `Arrow Down` - go down \
`D` or `Arrow Right` - go right

Demo: https://projects.juvarabrera.com/snake-game
